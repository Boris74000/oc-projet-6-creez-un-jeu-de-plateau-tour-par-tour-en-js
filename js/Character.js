"use strict";

class Character{
    constructor(nameCharacter, health, invocation, isCurrentPlayer, defenseMode) {
        this.nameCharacter = nameCharacter;
        this.health = health;
        this.invocation = invocation;
        this.isCurrentPlayer = isCurrentPlayer;
        this.defenseMode = defenseMode;
    }
}