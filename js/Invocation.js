"use strict";

class Invocation {
    constructor(nameInvocation, classNameInvocation, damage) {
        this.nameInvocation = nameInvocation;
        this.classNameInvocation = classNameInvocation;
        this.damage = damage;
    }
}